package org.liashenko.call_center.peak_interval;

import static org.junit.Assert.*;

import org.junit.Test;
import java.util.*;

public class MaxOverlappingIntervalSearchTest {

	private final Interval interval1 = new Interval(1385718405, 1385718491);
	private final Interval interval2 = new Interval(1385718405, 1385718492);
	private final Interval interval3 = new Interval(1385718415, 1385718511);

	@Test(expected = AssertionError.class)
	public void testEmptyIntervals(){

		MaxOverlappingIntervalSearch.search(null);
		MaxOverlappingIntervalSearch.search(Collections.emptyList());

	}

	@Test
	public void testValidSearch(){

		final OverlappingInterval result = MaxOverlappingIntervalSearch.search(Arrays.asList(interval1, interval2, interval3));

		assert(result.getStart() == 1385718415);
		assert(result.getEnd() == 1385718491);
		assert(result.getNumberOfOverlapping() == 3);

	}

}