package org.liashenko.call_center.peak_interval;

import static org.junit.Assert.*;

import org.junit.Test;
import java.util.*;

public class CallLogParserTest {
		
	@Test(expected = CallLogParserException.class)
	public void testInvalidLinesFormat() throws CallLogParserException {

		final List<String> lines = Arrays.asList("1385718405:1385718491", "1385718407");
		final List<Interval> intervals = CallLogParser.parse(lines);

	}	

	@Test(expected = CallLogParserException.class)
	public void testInvalidNumbers() throws CallLogParserException {

		final List<String> lines = Arrays.asList("1385718405:1385718491", "test:1385718491");
		final List<Interval> intervals = CallLogParser.parse(lines);

	}

	@Test
	public void testValidLines() throws CallLogParserException {

		final List<String> lines = Arrays.asList("1385718405:1385718491", "1385718408:1385718491");
		final List<Interval> intervals = CallLogParser.parse(lines);

	}

	@Test
	public void testEmptyLines() throws CallLogParserException {

		assertTrue(CallLogParser.parse(null).isEmpty());
		assertTrue(CallLogParser.parse(Arrays.asList()).isEmpty());

	}
}