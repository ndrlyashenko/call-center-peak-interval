package org.liashenko.call_center.peak_interval;

import static org.junit.Assert.*;

import org.junit.Test;

public class OverlappingIntervalTest {

	private final OverlappingInterval interval1 = new OverlappingInterval(1385718405, 1385718491, 1);
	private final OverlappingInterval interval2 = new OverlappingInterval(1385718405, 1385718491, 2);
	private final OverlappingInterval interval3 = new OverlappingInterval(1385718415, 1385718511, 2);

	@Test
	public void testToString() {

		assertEquals("1385718405:1385718491:1", interval1.toString());

	}

	@Test
	public void testEquals() {

		assertTrue(interval1.equals(interval2));
		assertFalse(interval1.equals(interval3));

	}

	@Test
	public void testHashCode() {

		assertTrue(interval1.hashCode() == interval2.hashCode());
		assertTrue(interval1.hashCode() != interval3.hashCode());

	}
}