package org.liashenko.call_center.peak_interval;

import static org.junit.Assert.*;

import org.junit.Test;

public class IntervalTest {
	

	private final Interval interval1 = new Interval(1385718405, 1385718491);
	private final Interval interval2 = new Interval(1385718405, 1385718491);
	private final Interval interval3 = new Interval(1385718415, 1385718511);

	@Test(expected = AssertionError.class)
	public void testNegativeInterval(){

		final Interval intervalInvalid = new Interval(-1, -1);

	}

	@Test(expected = AssertionError.class)
	public void testPointInterval(){

		final Interval intervalInvalid = new Interval(0, 0);

	}

	@Test
	public void testToString() {

		assertEquals("1385718405:1385718491", interval1.toString());

	}

	@Test
	public void testEquals() {

		assertTrue(interval1.equals(interval2));
		assertFalse(interval1.equals(interval3));

	}

	@Test
	public void testHashCode() {

		assertTrue(interval1.hashCode() == interval2.hashCode());
		assertTrue(interval1.hashCode() != interval3.hashCode());

	}
}