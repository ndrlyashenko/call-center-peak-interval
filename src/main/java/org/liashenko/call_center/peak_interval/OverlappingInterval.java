package org.liashenko.call_center.peak_interval;

/**
 * OverlappingInterval extends interval and contains number of interval overlapping.
 */
public class OverlappingInterval extends Interval {

	private final int numberOfOverlapping;

	public OverlappingInterval(long start, long end, int numberOfOverlapping) {
		super(start, end);
		this.numberOfOverlapping = numberOfOverlapping;
	}

	/**
	 * @return number of interval overlapping
	 */
	public long getNumberOfOverlapping() {
		return this.numberOfOverlapping;
	}

	/**
     * @see Object.toString()
     */
	@Override
	public String toString() {
		return this.getStart() + ":" + this.getEnd() + ":" + this.numberOfOverlapping;
	}   
}