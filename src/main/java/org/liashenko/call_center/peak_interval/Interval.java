package org.liashenko.call_center.peak_interval;

/**
 * This immutable datatype represents timestamp interval [{@code start}:{@code end}].
 */
public class Interval {

	private final long start;
	private final long end;
	
	/**
	 * {@code start} and {@code end} represent the timestamps of the call start and the call ending respectively. 
	 * {@code end} is always superior to {@code start}
	 * @param start represents left border of interval
	 *		  must be non-negative
	 * @param end represents right border of interval
	 *		  must be non-negative
	 */
	public Interval(long start, long end) {
		this.start = start;
		this.end = end;
		checkRep();
	}

	private void checkRep() {
		assert(start >= 0);
		assert(end >= 0);
		assert(end > start);
	}

	/**
	 * @return start of interval
	 */
	public long getStart() {
		return this.start;
	}

	/**
	 * @return end of interval
	 */
	public long getEnd() {
		return this.end;
	}
	
	/**
     * @see Object.toString()
     */
	@Override
	public String toString() {
		return this.start + ":" + this.end;
	}   

	/**
     * @see Object.equals()
     */
    @Override public boolean equals(Object thatObject) {
        if (!(thatObject instanceof Interval)) {
            return false;
        }

        Interval that = (Interval) thatObject;
        return this.start == that.start && this.end == that.end;
    }

    /**
     *  @see Object.hashCode()
     */
    @Override public int hashCode() {
    	final int bitsInInt = 32;
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(start^(start >> bitsInInt));
        result = prime * result + (int)(end^(end >> bitsInInt));
        return result;
    }
}