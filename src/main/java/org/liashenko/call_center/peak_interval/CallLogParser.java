package org.liashenko.call_center.peak_interval;

import java.io.*;
import java.util.*;

/**
 * CallLogParser parses logs.
 */	
public class CallLogParser {

	/**  
	 * Parse {@code lines} into list of {@code intervals} and return it.
	 * 
	 * @param  lines must be structured as follows: [{@code start}:{@code end}]
	 * @return list of {@code intervals}.
	 * @throws CallLogParserException if lines are not structured in specified format.
	 */	
	public static List<Interval> parse(List<String> lines) throws CallLogParserException {
		
		if(lines == null || lines.isEmpty()) {
			return Collections.emptyList();
		}

		List<Interval> intervals = new ArrayList<>();
		for(String line : lines) {				
			final String[] timestamps = line.split(":");
			if(timestamps.length < 2) {
				throw new CallLogParserException();
			}
			try {
				intervals.add(new Interval(Long.parseLong(timestamps[0]), Long.parseLong(timestamps[1])));				
			}catch (NumberFormatException e) {
				throw new CallLogParserException();
			}	
		}	
		return intervals; 	
	}

}