package org.liashenko.call_center.peak_interval;

import java.util.*;
import java.util.Arrays;
import java.util.stream.*;

/**
 * MaxOverlappingIntervalSearch provides search of interval with maximum number of occurance among list of intervals.
 */
public class MaxOverlappingIntervalSearch {

	public static OverlappingInterval search(List<Interval> intervals) {

  		assert(intervals != null);
  		assert(!intervals.isEmpty());

		final long[] startTimestamps = intervals.stream().map(Interval::getStart).mapToLong(l -> l).toArray();
		final long[] endTimestamps = intervals.stream().map(Interval::getEnd).mapToLong(l -> l).toArray();

		return search(startTimestamps, endTimestamps);
	}

	/**
	 * Search max overlapping interval and its frequency.	 
	 *
	 * Sort starting points and ending points in ascending order separately.
	 * Do merge of start and end by maintaining two pointers i and j respectively in the two arrays.
	 * If start[i] < end[i] : 
	 * 	a new range begins, increment the current counter and update value of max counter and interval limits.
	 * Otherwise : 
	 * 	it’s an end point of a range so we decrement the counter.
	 * 
	 * Complexity: O(n*lgn)
	 * 
	 * @param start array of intervals starts
	 * @param end   array of intervals ends
	 */
	private static OverlappingInterval search(long[] start, long[] end) {

		Arrays.sort(start);
		Arrays.sort(end);

		int currentOverlap = 0;
		int maxOverlap = 0;
		long startMaxOverlap = 0;
		long endMaxOverlap = 0;
		
		for(int i = 0, j = 0; i < start.length && j < end.length; ){
			if(start[i] < end[j]){
				currentOverlap++;
				if(currentOverlap > maxOverlap){
					maxOverlap = currentOverlap;
					startMaxOverlap = start[i];
					endMaxOverlap = end[j];
				}
				i++;				
			}
			else{
				currentOverlap--;
				j++;
			}
		}		

		return new OverlappingInterval(startMaxOverlap, endMaxOverlap, maxOverlap);

	}
}