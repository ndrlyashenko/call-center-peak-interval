package org.liashenko.call_center.peak_interval;

import java.util.*;
import java.io.*;
import java.nio.file.*;	
import java.util.stream.*;

/** 
 * This is the main application.
 */
public class PeakIntervalApplication {

	/**
     * Main method of the program. Reads file, fetches a list of intervals.
     * Search max overlapping interval and prints it.
     * 
	 * @param  args        command-line arguments (not used).
	 * @throws IOException if some error occured while reading file.
	 * @throws CallLogParserException if call logs file is bad formatted.
	 */
  	public static void main(String[] args) throws IOException, CallLogParserException {

  		final List<String> lines = Files.lines(Paths.get("call_logs.txt")).collect(Collectors.toList());
  		final List<Interval> intervals = CallLogParser.parse(lines);
  		final OverlappingInterval maxOverlappingInterval = MaxOverlappingIntervalSearch.search(intervals);
  		
		System.out.printf( 
			"The peak for this call log is %d simultaneous calls, that occured between %d and %d.\n",
			maxOverlappingInterval.getNumberOfOverlapping(), 
			maxOverlappingInterval.getStart(),
			maxOverlappingInterval.getEnd()
		);
  	}

}